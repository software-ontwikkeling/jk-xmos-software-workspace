/*
 * wm8805.h
 *
 *  Created on: 26 okt. 2019
 *      Author: JKacoustics
 */


#ifndef WM8805_H_
#define WM8805_H_

#include "debug_print.h"
#include "device_reg.h"
#include "i2c.h"
#include "gpio.h"
#include <timer.h>

typedef struct RECV_WM8805 {
    int i2c_addr;
    device_reg RST;     //R0
    device_reg PLL1;    //R3
    device_reg PLL2;    //R4
    device_reg PLL3;    //R5
    device_reg PLL4;    //R6
    device_reg PLL5;    //R7
    device_reg PLL6;    //R8
    device_reg SPDMODE; //R9
    device_reg INTMASK; //R10
    device_reg INTSTAT; //R11
    device_reg SPDSTAT;
    device_reg RXCHAN4;
    device_reg GPO01;   //R23
    device_reg AIFRX;   //R28
    device_reg SPDRX1;  //R29
    device_reg PWRDN;   //R30
} RECV_WM8805;

void wm8805_set_software_mode(port reset, port gpo0, port sdin);
void wm8805_init(client interface i2c_master_if i2c);
void wm8805_task(client interface i2c_master_if i2c, client interface input_gpio_if gpo0);

#endif /* WM8805_H_ */
