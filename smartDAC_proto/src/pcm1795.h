/*
 * pcm1795.h
 *
 *  Created on: 19 okt. 2019
 *      Author: JKacoustics
 */


#ifndef PCM1795_H_
#define PCM1795_H_

#include "debug_print.h"
#include "device_reg.h"
#include "i2c.h"

typedef struct DAC_PCM1795 {
    int i2c_addr;
    device_reg r18;
    device_reg r19;
    device_reg r20;
} DAC_PCM1795;

// Register 18
#define ATLD    7
#define FMT_2   6
#define FMT_1   5
#define FMT_0   4
#define DMF_1   3
#define DMF_0   2
#define DME     1
#define MUTE    0

// Register 19
#define REV     7
#define ATS_1   6
#define ATS_0   5
#define OPE     4
#define RSV     3
#define DFMS    2
#define FLT     1
#define IZND    0

// Register 20
#define SRST    6
#define DSD     5
#define DFTH    4
#define MONO    3
#define CHSL    2
#define OS_1    1
#define OS_0    0

void pcm1795_task(client interface i2c_master_if i2c);


#endif /* PCM1795_H_ */
