/*
 * wm8805.xc
 *
 *  Created on: 19 okt. 2019
 *      Author: JKacoustics
 */

#include "wm8805.h"
#include <timer.h>
#include <xs1.h>

#define WM8805_I2C_WRITE_ADDR 0x3A

RECV_WM8805 RECV = {
    WM8805_I2C_WRITE_ADDR,  // i2c_addr
    {0x00},                 // device_reg RST;     //read-only
    {0x03, 0x21},           // device_reg PLL1;    //00100001
    {0x04, 0xFD},           // device_reg PLL2;    //11111101
    {0x05, 0x36},           // device_reg PLL3;    //00110110
    {0x06, 0x07},           // device_reg PLL4;    //00000111
    {0x07, 0x16},           // device_reg PLL5;    //00010110
    {0x08, 0x18},           // device_reg PLL6;    //00011000
    {0x09, 0xFF},           // device_reg SPDMODE; //11111111
    {0x0A, 0x00},           // device_reg INTMASK; //00000000
    {0x0B},                 // device_reg INTSTAT; //read-only
    {0x0C},                 // device_reg SPDSTAT; //read-only
    {0x10},					// device_reg RXCHAN4; //read-only
    {0x17, 0x70},           // device_reg GPO01;   //01110000
    {0x1C, 0x06},           // device_reg AIFRX;   //00000110
    {0x1D, 0x80},           // device_reg SPDRX1;  //10000000
    {0x1E, 0x07}            // device_reg PWRDN;   //00000111
};

void wm8805_set_software_mode(port reset, port gpo0, port sdin){
    int x;                  // dummy variable
    reset <: 0x00;          // pull reset low
    gpo0 <: 0x00;           // pull gpo0 low
    delay_milliseconds(1);
    reset :> x;             // release reset
    delay_milliseconds(1);
    gpo0 :> x;              // release gpo0
}

bool wm8805_read_deviceid(client i2c_master_if i2c){
    i2c_regop_res_t res;

    uint8_t id1 = i2c.read_reg(RECV.i2c_addr, 0x00, res);
    if (res != I2C_REGOP_SUCCESS)
        return false;

    uint8_t id2 = i2c.read_reg(RECV.i2c_addr, 0x01, res);
    if (res != I2C_REGOP_SUCCESS)
        return false;

    if (id1 == 0x05 && id2 == 0x88)
    {
        debug_printf("WM8805 STATUS OK\n");
        return true;
    }

    return false;
}



uint8_t wm8805_determine_sample_rate(uint8_t spdstat/*client i2c_master_if i2c*/){
    //i2c_regop_res_t res;
    uint8_t sample_freq = 0;

    /*
    uint8_t spdstat = i2c.read_reg(RECV.i2c_addr, RECV.SPDSTAT.addr, res);
    if (res != I2C_REGOP_SUCCESS)
    {
        debug_printf("I2C failed to read register %d from 0x%02X\n", RECV.SPDSTAT.addr, RECV.i2c_addr);
    }
    else
    {
        debug_printf("WM8805 SPDSTAT: 0x%02X\n", spdstat);
        if (spdstat & (1 << 6))
        {
            debug_printf("WM8805 UNLOCKED, checking sample frequency aborted...\n");
            return;
        }
    }*/

    /*if (spdstat & (1 << 6))
    {
        debug_printf("WM8805 UNLOCKED, checking sample frequency aborted...\n");
        return 0;
    }*/

    if ((spdstat & (1 << 5)) && (spdstat & (1 << 4)))
        sample_freq = 32;
    else if ((spdstat & (1 << 5)) && (!(spdstat & (1 << 4))))
        sample_freq = 44;
    else if ((spdstat & (1 << 4)) && (!(spdstat & (1 << 5))))
        sample_freq = 96;
    else //if ((!(spdstat & (1 << 4))) && (!(spdstat & ( 1 << 5))))
        sample_freq = 192;

    debug_printf("WM8805 Sample Rate: %d kHz\n", sample_freq);

    return sample_freq;
}

bool wm8805_pll_check_default(){
    if (RECV.PLL1.data == 0x21 && RECV.PLL2.data == 0xFD &&
            RECV.PLL3.data == 0x36 && RECV.PLL4.data == 0x07)
        return true;

    return false;
}

void wm8805_pll_write_current_settings(client i2c_master_if i2c){
    bool success = true;
    int res = i2c.write_reg(RECV.i2c_addr, RECV.PLL1.addr, RECV.PLL1.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PLL2.addr, RECV.PLL2.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PLL3.addr, RECV.PLL3.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PLL4.addr, RECV.PLL4.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    /*if (success)
        debug_printf("WM8805 PLL successfully updated...\n");
    else
        debug_printf("WM8805 PLL update error...\n");*/
    if (!success)
        debug_printf("WM8805 PLL update error...\n");
    else
        debug_printf("WM8805 PLL successfully updated...\n");
}

void wm8805_pll_settings_192khz(){
    RECV.PLL1.data = 0xBA;
    RECV.PLL2.data = 0x49;
    RECV.PLL3.data = 0x0C;
    RECV.PLL4.data = 0x08;
}

void wm8805_pll_settings_default(){
    RECV.PLL1.data = 0x21;
    RECV.PLL2.data = 0xFD;
    RECV.PLL3.data = 0x36;
    RECV.PLL4.data = 0x07;
}

uint8_t wm8805_read_intstat(client i2c_master_if i2c){
    i2c_regop_res_t res;

	uint8_t intstat = i2c.read_reg(RECV.i2c_addr, RECV.INTSTAT.addr, res);
	if (res != I2C_REGOP_SUCCESS)
	{
		debug_printf("I2C failed to read register\n");
		return 0;
	}

	debug_printf("WM8805 INTSTAT: 0x%02X\n", intstat);
	return intstat;
}

uint8_t wm8805_read_spdstat(client i2c_master_if i2c){
    i2c_regop_res_t res;

    uint8_t spdstat = i2c.read_reg(RECV.i2c_addr, RECV.SPDSTAT.addr, res);
    if (res != I2C_REGOP_SUCCESS){
        debug_printf("I2C failed to read register\n");
        return 0;
    }

    debug_printf("WM8805 SPDSTAT: 0x%02X\n", spdstat);
    return spdstat;
}

void wm8805_lock_control2(client i2c_master_if i2c, client input_gpio_if gpo0)
{
    debug_printf("WM8805 ATTEMPT 2!\n");
    delay_milliseconds(6);

    // read register 11 & 12
  	uint8_t intstat = wm8805_read_intstat(i2c);
    uint8_t spdstat = wm8805_read_spdstat(i2c);

    if (!(intstat & (1 << 0)) && (spdstat & (1 << 6)))
    {
    	wm8805_determine_sample_rate(spdstat);
    	return;
    }
    else if (!(intstat & (1 << 0)) || !(spdstat & (1 << 6)))
    {
    	uint8_t sf = wm8805_determine_sample_rate(spdstat);
    	if (wm8805_pll_check_default() == false && sf != 192)
		{
			wm8805_pll_settings_default();
			debug_printf("WM8805 Writing PLL Settings for default...\n");
		}
    	wm8805_pll_write_current_settings(i2c);
    	return;
    }



    // still not locked
    if (wm8805_pll_check_default())
    {
        wm8805_pll_settings_192khz();
        debug_printf("WM8805 Writing PLL Settings for 192kHz...\n");
    }
    else
    {
        wm8805_pll_settings_default();
        debug_printf("WM8805 Writing PLL Settings for default...\n");
    }
    wm8805_pll_write_current_settings(i2c);
}

void wm8805_lock_control(client i2c_master_if i2c, client input_gpio_if gpo0)
{
	//bool locked = false;
  	debug_printf("WM8805 GPO0 interrupt!\n");
  	delay_milliseconds(1);

    // read register 11 & 12
  	uint8_t intstat = wm8805_read_intstat(i2c);
    uint8_t spdstat = wm8805_read_spdstat(i2c);

    if (intstat & (1 << 7)){
    	uint8_t sf = wm8805_determine_sample_rate(spdstat);
    	if (wm8805_pll_check_default() == false && sf != 192)
		{
			wm8805_pll_settings_default();
			debug_printf("WM8805 Writing PLL Settings for default...\n");
			wm8805_pll_write_current_settings(i2c);
		}
    	return;
    }

    if (!(intstat & (1 << 0)))
    	return;

    if (!(spdstat & (1 << 6)))
    {
        // read register 12
    	uint8_t spdstat = wm8805_read_spdstat(i2c);
    }

    if (spdstat & (1 << 6))
    {
        if (wm8805_pll_check_default())
        {
            wm8805_pll_settings_192khz();
            debug_printf("WM8805 Writing PLL Settings for 192kHz...\n");
            //wm8805_pll_write_current_settings(i2c);
        }
        else
        {
            wm8805_pll_settings_default();
            debug_printf("WM8805 Writing PLL Settings for default...\n");
        }
        wm8805_pll_write_current_settings(i2c);
    }
    else
    {
    	uint8_t sf = wm8805_determine_sample_rate(spdstat);
    	if (wm8805_pll_check_default() == false && sf != 192)
		{
			wm8805_pll_settings_default();
			debug_printf("WM8805 Writing PLL Settings for default...\n");
		}
    	wm8805_pll_write_current_settings(i2c);
    }
    wm8805_lock_control2(i2c, gpo0);
}


void wm8805_init(client i2c_master_if i2c){
    if (!wm8805_read_deviceid(i2c))
    {
        debug_printf("WM8805 STATUS NOT OK\n");
        return;
    }

    debug_printf("WM8805 initializing...\n");
    RECV.RST.data       = 0x00; // Apply reset on device
    RECV.PLL5.data      = 0x04; // Use PLL in fractional mode so both PLL_K and PLL_N values are used
    RECV.PLL6.data      = 0x11; // Select input RX1 to pass to receiver, enable clock out pin and fill mode = 0
    RECV.SPDMODE.data   = 0x00; // Select all inputs to be CMOS inputs
    RECV.INTMASK.data   = 0x7E; // Mask errors: INT_INVALID, INT_CSUD, INT_TRANS_ERR, UPD_CPY_N, UDP_DEEMPH
    RECV.AIFRX.data     = 0x4E; // Audio data format = I�S, Word length = 24 bits, WM8805 is a master
    RECV.GPO01.data     = 0x70; // GPO0 and GPO1 both indicate a change in sample frequency
    RECV.PWRDN.data     = 0x04; // Power up PLL, Power up receiver, Power down transmitter, Power up oscillator, Power up Digital Audio Interface

    bool success = true;
    int res = i2c.write_reg(RECV.i2c_addr, RECV.RST.addr, RECV.RST.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PLL5.addr, RECV.PLL5.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PLL6.addr, RECV.PLL6.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.SPDMODE.addr, RECV.SPDMODE.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.INTMASK.addr, RECV.INTMASK.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.AIFRX.addr, RECV.AIFRX.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.SPDRX1.addr, RECV.SPDRX1.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.GPO01.addr, RECV.GPO01.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    res = i2c.write_reg(RECV.i2c_addr, RECV.PWRDN.addr, RECV.PWRDN.data);
    if (res != I2C_REGOP_SUCCESS)
        success = false;

    //wm8805_pll_settings_192khz();
    wm8805_pll_settings_default();
    wm8805_pll_write_current_settings(i2c);

    if (success)
        debug_printf("OK\n");
    else
        debug_printf("NOT OK\n");
}

void wm8805_task(client i2c_master_if i2c, client input_gpio_if gpo0){
    wm8805_init(i2c);
    uint32_t lock_time;
    timer t;

    //i2c_regop_res_t res;
    gpo0.event_when_pins_eq(0);
    t :> lock_time;
    lock_time += XS1_TIMER_MHZ * 1000 * 1000;
    while(1)
    {
        // wait for interrupts and handle them
        select
        {
            case gpo0.event():
                if (gpo0.input() == 1)
                {
                    gpo0.event_when_pins_eq(0);
                }
                else
                {
                    wm8805_lock_control(i2c, gpo0);
                    t :> lock_time;
                    lock_time += XS1_TIMER_MHZ * 50 * 1000;
                    gpo0.event_when_pins_eq(1);
                }
                break;

            case t when timerafter(lock_time) :> void:
            	// read gpo0, fire wm8805 lock control if gpo0 is low
            	if (gpo0.input() == 0){
            		wm8805_lock_control(i2c, gpo0);
            		lock_time += XS1_TIMER_MHZ * 50 * 1000;
            	}

            	lock_time += XS1_TIMER_MHZ * 1000 * 1000;
            	break;
        }
    }
}
