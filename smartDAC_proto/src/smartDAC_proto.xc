/*
 * smartDAC_proto.xc
 *
 *  Created on: 19 okt. 2019
 *      Author: JKacoustics
 */

#include <platform.h>
#include <timer.h>
#include <xs1.h>
#include <stdio.h>

#include "debug_print.h"
#include "xassert.h"
#include "i2c.h"
#include "gpio.h"

#include "pcm1795.h"
#include "wm8805.h"

//port xmos_refclk = XS1_PORT_1M;
//port xmos_iis_lrclk = XS1_PORT_1O;
//port xmos_iis_bclk = XS1_PORT_1P;
//port xmos_iis_data = XS1_PORT_1N;

//port button = XS1_PORT_4C; //0
//port signal_ir = XS1_PORT_4C; //1
//port xmos_sel = XS1_PORT_4C; //2
//port wm_sel = XS1_PORT_4C; //3

out port iis_sel = XS1_PORT_4C;
//char iis_sel_pin_map = {3,2};

port gpo0 = XS1_PORT_4D;

//port gpo0 = XS1_PORT_4D; //0
//port mute = XS1_PORT_4D; //1
//port zeror_wm = XS1_PORT_4D; //2
//port zerol_wm = XS1_PORT_4D; //3

//port xmos_spdif_tx = XS1_PORT_1L;

port p_scl = XS1_PORT_1A;
port p_sda = XS1_PORT_1D;

port wm8805_reset = XS1_PORT_4E; // 0

#define XMOS    0
#define WM      1

void switch_iis_sel(int mode) {
    if (mode)
        iis_sel <: 0x04;
    else
        iis_sel <: 0x08;
}

int main () {
    debug_printf("Booting SmartDAC...\n");

    wm8805_set_software_mode(wm8805_reset, gpo0, p_sda);

    // declare interfaces
    i2c_master_if i2c[1];
    input_gpio_if i_gpo0[1];

    switch_iis_sel(WM);
    par {
        input_gpio_with_events(i_gpo0, 1, gpo0, null);

        i2c_master(i2c, 1, p_scl, p_sda, 400);
        wm8805_task(i2c[0], i_gpo0[0]);
        //pcm1795_task(i2c[0]);
    }
    return 0;
}
