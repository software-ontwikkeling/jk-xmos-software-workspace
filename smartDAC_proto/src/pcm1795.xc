/*
 * pcm1795.xc
 *
 *  Created on: 19 okt. 2019
 *      Author: JKacoustics
 */

#include "pcm1795.h"

#define I2C_WRITE_ADDR_DAC 0x98
#define I2C_READ_ADDR_DAC 0x99

DAC_PCM1795 DAC = {
        I2C_WRITE_ADDR_DAC, //_i2c_addr
        {0x12, 0x50},       // _reg18 //0101 0000
        {0x13, 0x00},       // _reg19 //0000 0000
        {0x14, 0x00}        // _reg20 //0000 0000
};

void pcm1795_init(){
  // moeten we initializeren op 32bits output?
    debug_printf("Initializing DAC...\n");
};

void pcm1795_switch_dsd(){
    //nazoeken wat hier moet gebeuren
    //bit 5 van R20 hoog zetten
    debug_printf("Switching DAC to DSD...\n");

    DAC.r18.data = 0xD0; // 32bit input
    DAC.r20.data = 0x20; // dsd mode

    // transmit here
};

void pcm1795_switch_pcm(){
    //nazoeken wat hier moet gebeuren
    //bit 5 van R20 laag zetten
    debug_printf("Switching DAC to PCM...\n");

    DAC.r20.data = 0x50; // default 24bit i2s
    DAC.r20.data = 0x00; // pcm mode

    //transmit here
};

void pcm1795_new_sample_rate(){
  // change oversampling to have consistent operating frequency? Nazoeken? is system clock 256fs?
};

void pcm1795_task(client i2c_master_if i2c){
    pcm1795_init();
    // lees 0x17
    i2c_regop_res_t result;
    uint8_t id = i2c.read_reg(I2C_WRITE_ADDR_DAC, 0x17, result);
    if (result != I2C_REGOP_SUCCESS)
        debug_printf("I2C read reg failed\n");
    debug_printf("%d\n", id);

    while(1) {
    };
};


