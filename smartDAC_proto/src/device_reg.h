/*
 * device_reg.h
 *
 *  Created on: 19 okt. 2019
 *      Author: JKacoustics
 */

#ifndef DEVICE_REG_H_
#define DEVICE_REG_H_

typedef struct device_reg {
    int addr;
    int data;
} device_reg;

typedef enum { false, true} bool;

#endif /* DEVICE_REG_H_ */
