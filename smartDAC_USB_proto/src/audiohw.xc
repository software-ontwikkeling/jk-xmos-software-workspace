/*
 * audiohw.xc
 *
 *  Created on: 9 nov. 2019
 *      Author: JKacoustics
 */

#include <xs1.h>
#include <assert.h>
#include "devicedefines.h"
#include <platform.h>
#include "i2c_shared.h"
#include "print.h"
#include "dsd_support.h"

#define DAC_I2C_ADDR 0x48
#define ADC_I2C_ADDR 0x5A

#ifndef IAP
    /* If IAP not enabled, i2c ports not declared - still needs for DAC config */
    on tile [0] : struct r_i2c r_i2c = { XS1_PORT_4A };
#else
    extern struct r_i2c r_i2c;
#endif

#define DAC_REGWRITE(reg, val) {data[0] = val; i2c_shared_master_write_reg(r_i2c, DAC_I2C_ADDR, reg, data, 1);}
#define DAC_REGREAD(reg, val) {i2c_shared_master_read_reg(r_i2c, DAC_I2C_ADDR, reg, val, 1);}
#define ADC_REGWRITE(reg, val) {data[0] = val; i2c_shared_master_write_reg(r_i2c, ADC_I2C_ADDR, reg, data, 1);}

void AudioHwInit(chanend ?c_codec)
{
    unsigned char data[2] = { 0, 0 };
    /* Init the i2c module */
    i2c_shared_master_init(r_i2c);
    // To do: initialise the audio hardware here including PLL selection, ADC (if any) and DAC (if any).
}

/* Configures the external audio hardware for the required sample frequency.*/
void AudioHwConfig(unsigned samFreq, unsigned mClk, chanend ?c_codec, unsigned dsdMode, unsigned sampRes_DAC, unsigned sampRes_ADC)
{
    unsigned char data[1] = { 0 };
    // To do: Handle the events listed below and reconfigure the PLL, ADC (if any) and DAC (if any)
    // (1) sample rate change
    // (2) PCM/DSD mode switching
    return;
}
